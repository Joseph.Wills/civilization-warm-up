##//////////////////////////////////////////////////////////////////##
##                                                                  ##
## Main file to be run if we don't want to/get to using setup tools ##
## to package the project. __main__.py can be called by navigating  ##
## to civilization-warm-up/civilization and calling                 ##
## python3 __main__.py                                              ##
##                                                                  ##
## or                                                               ##
##                                                                  ##
## navigating to civilization-warm-up and calling                   ##
## python3 -m civilization                                          ##
##                                                                  ##
##//////////////////////////////////////////////////////////////////##
from civilization.interfaces.commandline.commandline import run_commandline


if __name__ == '__main__':
    run_commandline()
