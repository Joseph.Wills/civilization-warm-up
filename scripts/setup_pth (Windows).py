##//////////////////////////////////////////////////////////////////##
## Script to set up pth file for virtual environment to recognize   ##
## project packages.                                                ##
##//////////////////////////////////////////////////////////////////##
import os
from pathlib import Path

# Path to site packages in virtual environment
PATH_TO_SITE_PACKAGES = 'Lib/site-packages'

# Get name of virtual environment and path to project root
ENVIRONMENT_NAME = input('Enter virtual environment directory: ')
ROOT_PATH = str(Path(os.getcwd()).parent)

# Combine paths to get path to pth file
PTH_PATH = os.path.join(ROOT_PATH, ENVIRONMENT_NAME, PATH_TO_SITE_PACKAGES, 'path.pth')

# Write project root path to path.pth
with open(PTH_PATH, 'w') as path_file:
    path_file.write(ROOT_PATH)
