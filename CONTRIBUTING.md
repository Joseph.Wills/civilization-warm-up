# Project Structure

## civilization
Main directory for project code 

##### database_interaction
Directory containing all components for creating and interacting with
the database

##### datasets
Directory containing csv files of Artist and Song datasets

##### interfaces
Directory containing all components for user interface

##### parsing
Directory containing all components for parsing