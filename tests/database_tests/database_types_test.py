##//////////////////////////////////////////////////////////////////##
##  Test all database types and ensure they are working correctly   ##
##//////////////////////////////////////////////////////////////////##
import civilization.database_interaction.database_types as dt
import unittest


class TestDatabaseTypes(unittest.TestCase):

    def setUp(self):
        self.Modes = dt.Modes
        self.Tables = dt.Tables

    def test_modes_type(self):
        ## Ensure modes display correct type ##
        self.assertTrue(type(self.Modes.LOAD.value) == str)
        self.assertTrue(type(self.Modes.QUERY.value) == str)

    def test_tables_type(self):
        ## Ensure tables display correct type ##
        self.assertTrue(type(self.Tables.ARTISTS.value) == str)
        self.assertTrue(type(self.Tables.SONGS.value) == str)

    def test_modes_data(self):
        ## Ensure tables display correct data ##
        self.assertTrue(self.Modes.LOAD.value == 'LOAD')
        self.assertTrue(self.Modes.QUERY.value == 'QUERY')

    def test_tables_data(self):
        ## Ensure tables display correct data ##
        self.assertTrue(self.Tables.ARTISTS.value == 'ARTISTS')
        self.assertTrue(self.Tables.SONGS.value == 'SONGS')


if __name__ == '__main__':
    unittest.main()
