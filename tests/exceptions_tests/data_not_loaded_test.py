##//////////////////////////////////////////////////////////////////##
## Test DataNotLoaded Exception to ensure it is raised and          ##
## displays correct message.                                        ##
##//////////////////////////////////////////////////////////////////##
from civilization.interfaces.commandline.exceptions import DataNotLoaded
import unittest

NOT_LOADED_MSG = "ERROR: Database has not been loaded. "   \
                 "Call 'load data' to load it before making " \
                 "a query"


class TestDataNotLoadedException(unittest.TestCase):

    def setUp(self):
        self.data_not_loaded_test = DataNotLoaded

    def raise_DataNotLoaded(self):
        ## Manually raise Exception ##
        try:
            raise self.data_not_loaded_test
        except self.data_not_loaded_test as test:
            return str(test)

    def test_is_raised(self):
        ## Ensure Exception is raised when expected ##
        with self.assertRaises(self.data_not_loaded_test):
            raise self.data_not_loaded_test

    def test_displays_correct_message(self):
        ## Ensure correct message is displayed ##
        ## when raised                         ##
        self.assertEquals(self.raise_DataNotLoaded(), NOT_LOADED_MSG)


if __name__ == '__main__':
    unittest.main()