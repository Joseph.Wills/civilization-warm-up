##//////////////////////////////////////////////////////////////////##
## Test InvalidCommand Exception to ensure it is raised and         ##
## displays correct message.                                        ##
##//////////////////////////////////////////////////////////////////##
from civilization.interfaces.commandline.exceptions import InvalidCommand
import unittest

INVALID_MSG = "ERROR: Command not valid. See 'help' for more info"


class TestInvalidCommandException(unittest.TestCase):

    def setUp(self):
        self.query_not_found_test = InvalidCommand

    def raise_InvalidCommand(self):
        ## Manually raise Exception ##
        try:
            raise self.query_not_found_test
        except self.query_not_found_test as test:
            return str(test)

    def test_is_raised(self):
        ## Ensure Exception is raised when expected ##
        with self.assertRaises(self.query_not_found_test):
            raise self.query_not_found_test

    def test_displays_correct_message(self):
        ## Ensure correct message is displayed ##
        ## when raised                         ##
        self.assertEquals(self.raise_InvalidCommand(), INVALID_MSG)


if __name__ == '__main__':
    unittest.main()