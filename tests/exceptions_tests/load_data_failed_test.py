##//////////////////////////////////////////////////////////////////##
## Test LoadDataFailed Exception to ensure it is raised and         ##
## displays correct message.                                        ##
##//////////////////////////////////////////////////////////////////##
from civilization.interfaces.commandline.exceptions import LoadDataFailed
import unittest

FAIL_MSG = "ERROR: Load Data Failed"


class TestLoadDataFailedException(unittest.TestCase):

    def setUp(self):
        self.load_data_failed_test = LoadDataFailed

    def raise_LoadDataFailed(self):
        ## Manually raise Exception ##
        try:
            raise self.load_data_failed_test
        except self.load_data_failed_test as test:
            return str(test)

    def test_is_raised(self):
        ## Ensure Exception is raised when expected ##
        with self.assertRaises(self.load_data_failed_test):
            raise self.load_data_failed_test

    def test_displays_correct_message(self):
        ## Ensure correct message is displayed ##
        ## when raised                         ##
        self.assertEquals(self.raise_LoadDataFailed(), FAIL_MSG)


if __name__ == '__main__':
    unittest.main()