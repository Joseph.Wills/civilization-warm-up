##//////////////////////////////////////////////////////////////////##
##  Test Quit Exception to ensure it is raised and displays correct ##
##  message.                                                        ##
##//////////////////////////////////////////////////////////////////##
from civilization.interfaces.commandline.exceptions import Quit
import unittest


QUIT_MSG = "See ya later :)"


class TestQuitException(unittest.TestCase):

    def setUp(self):
        self.quit_test = Quit

    def raise_Quit(self):
        ## Manually raise Exception ##
        try:
            raise self.quit_test
        except self.quit_test as test:
            return str(test)

    def test_is_raised(self):
        ## Ensure Exception is raised when expected ##
        with self.assertRaises(self.quit_test):
            raise self.quit_test

    def test_displays_correct_message(self):
        ## Ensure correct message is displayed ##
        ## when raised                         ##
        self.assertEquals(self.raise_Quit(), QUIT_MSG)


if __name__ == '__main__':
    unittest.main()