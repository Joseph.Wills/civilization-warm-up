##//////////////////////////////////////////////////////////////////##
## Test ArgsNotSpecified Exception to ensure it is raised and       ##
## displays correct message.                                        ##
##//////////////////////////////////////////////////////////////////##
from civilization.interfaces.commandline.exceptions import ArgsNotSpecified
import unittest


def test1(arg1):
    ## Fake testing function ##
    pass


def test2(arg2):
    ## Fake testing function ##
    pass


def test3(arg3):
    ## Fake testing function ##
    pass


class TestArgsNotSpecifiedException(unittest.TestCase):

    def setUp(self):
        self.args_not_specified_test = ArgsNotSpecified
        self.messages = [
            "ERROR: arg1 not specified in test1",
            "ERROR: arg2 not specified in test2",
            "ERROR: arg3 not specified in test3"
        ]

    def raise_ArgsNotSpecified(self, arg, function):
        ## Manually raise Exception ##
        try:
            raise self.args_not_specified_test(arg, function)
        except self.args_not_specified_test as test:
            return str(test)

    def test_is_raised(self):
        ## Ensure Exception is raised when expected ##
        with self.assertRaises(self.args_not_specified_test):
            raise self.args_not_specified_test('fake_arg', 'fake_func')

    def test_displays_correct_message(self):
        ## Ensure correct message is displayed ##
        ## when raised                         ##
        self.assertEqual(self.raise_ArgsNotSpecified('arg1', test1.__name__), self.messages[0])
        self.assertEqual(self.raise_ArgsNotSpecified('arg2', test2.__name__), self.messages[1])
        self.assertEqual(self.raise_ArgsNotSpecified('arg3', test3.__name__), self.messages[2])


if __name__ == '__main__':
    unittest.main()
