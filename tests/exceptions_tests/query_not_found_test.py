##//////////////////////////////////////////////////////////////////##
## Test QueryNotFound Exception to ensure it is raised and          ##
## displays correct message.                                        ##
##//////////////////////////////////////////////////////////////////##
from civilization.interfaces.commandline.exceptions import QueryNotFound
import unittest

NOT_FOUND_MSG = "ERROR: Query not found"


class TestQueryNotFoundException(unittest.TestCase):

    def setUp(self):
        self.query_not_found_test = QueryNotFound

    def raise_QueryNotFound(self):
        ## Manually raise Exception ##
        try:
            raise self.query_not_found_test
        except self.query_not_found_test as test:
            return str(test)

    def test_is_raised(self):
        ## Ensure Exception is raised when expected ##
        with self.assertRaises(self.query_not_found_test):
            raise self.query_not_found_test

    def test_displays_correct_message(self):
        ## Ensure correct message is displayed ##
        ## when raised                         ##
        self.assertEquals(self.raise_QueryNotFound(), NOT_FOUND_MSG)


if __name__ == '__main__':
    unittest.main()