# Civilization Database Parser
Civilization is a professional Database Parser and Query Language built to interact with a relational database containing 
Spotify's most streamed songs in 2019. It features a custom built commandline interface and parser and implements 
sqlite3 to interact with the database. Civilization Query Language (CQL) is well designed and provides a sleek,
fluid experience for users of all expertise. 


# Running:
Navigate to parent directory where Civilization is installed and run: <b>python3 civilization-warm-up</b>


# Database
The database contains two tables: Artists and Songs.


### Artists                 
---
| Title          | Type                               |
| -------------- | ---------------------------------- |
| id [rank]      | int                                |
| name           | string                             |
| genre          | string                             |
| song(s) name   | int(s) [foreign key to song table] |


### Songs 
---
| Title          | Type                                 |
| -------------- | ------------------------------------ |
| id [rank]      | int                                  |
| name           | string                               |
| length         | int                                  |
| artist_name    | int [foreign key to artist table]    |


# Civilization Query Language (CQL)

#### Give rank of given element:

rank artist [artist name]

rank song [song name]


#### Give genre of given element:

genre artist [artist name]

genre song [song name]


#### Give name of song or artist of given rank:

name rank song [number]

name rank artist [number]


#### Give all elements of given genre:

name genre song [genre name]

name genre artist [genre name]


#### Give length of song:

length song [song name]

#### Give all songs of a given length

name length song [length]


#### Display command help:

help

#### Load the database:

load data

#### Quit:

quit