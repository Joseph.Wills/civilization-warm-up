from civilization.parsing.command_splitter import split_command
from civilization.interfaces.commandline.exceptions import Quit
from civilization.interfaces.commandline.exceptions import InvalidCommand

commands = ["rank artist", "rank song", "genre artist", "genre song", "name rank song", "name rank artist",
            "name genre song", "name genre artist", "length song", "name length song", "help", "load data", "quit"]
commands_desc = {"rank artist": "gives rank of artist", "rank song": "gives rank of song",
                 "genre artist": "gives genre of artist", "genre song": "gives genre of song",
                 "name rank song": "gives name of song of given rank",
                 "name rank artist": "gives name of artist of given rank",
                 "name genre song": "gives all songs of given genre",
                 "name genre artist": "gives all artists of given genre", "length song": "gives length of song",
                 "name length song": "gives all songs of a given length",
                 "help": "Get help, or get help with a specific keyword", "load data": "Load data",
                 "quit": "Quit the program"}

'''
Gives help. If keywords are given after the keyword "help", it prints the commands which contain that keyword..
'''


def help(args):
    index = 0
    message = ''
    try:
        to_help = args[1]#args.split(" ", 1)[1]
        for command in commands:
            if to_help in command:
                index += 1
                message += str(index) + "): " + command + " : " + commands_desc[command]
                message += '\n'
        return message

    except:
        print("General help:")
        for command in commands:
            index += 1
            message += str(index) + "): " + command + " : " + commands_desc[command]
            message += '\n'
        return message

def help_errors(args):
    s = ""
    for arg in args:
        s += " " + arg
    x = 0
    possible_commands = []
    for c in commands:
        if c in s:
            x += 1
            possible_commands.append(str(x) + "): " + c + " _ : " + commands_desc[c])
    if len(possible_commands) > 0:
        print("Were you trying to use one of these commands?")
        for command in possible_commands:
            print(command)


def parser_function(command, database_operator):
    #orig_args = command.split()
    # List of args that have been split and formatted
    args = split_command(command)


    if args[0] == "quit":
        raise Quit
    if args[0] == "help":
        return help(args)
    if args[0] == 'load':
        return database_operator.load()

    if len(args) == 1:
        #return "Incorrect commands entered"
        raise InvalidCommand

    elif args[0] == "rank":
        if args[1] == "artist":
            # this is stored in arg[2] search query for arg3[artist name]
            # return the rank of given element
            return database_operator.query('ID', 'ARTISTS', 'NAME', args[2])
    #elif args[0] == "rank":
        if args[1] == "song":
            # this is stored is arg[3] , search query for arg3 [song name]
            # return the rank of given element
            return database_operator.query('ID', 'SONGS', 'NAME', args[2])

    elif args[0] == "genre":
        if args[1] == "artist":
            # this is stored in args[3], search query for arg3[artist name]
            # return the genre
            return database_operator.query('GENRE', 'ARTISTS', 'NAME', args[2])
    #elif args[0] == "genre":
        if args[1] == "song":
            # this is stored in arg3,search query for arg3 [artist name]
            # return the genre
            ##### This is a foreign query one ######
            return database_operator.foreign_query('GENRE', 'ARTISTS', 'NAME', args[2])

    elif args[0] == "name":
        if args[1] == "rank":
            if args[2] == "song":
                # this is stored in arg4 , search for arg4 [number]
                # return the song at the given rank
                return database_operator.query('NAME', 'SONGS', 'ID', args[3])
    #elif args[0] == "name":
        if args[1] == "rank":
            if args[2] == "artist":
                # this is stored in arg4 , search query for arg4[number]
                # return the artist at the given rank
                return database_operator.query('NAME', 'ARTISTS', 'ID', args[3])
    #elif args[0] == "name":
        if args[1] == "genre":
            if args[2] == "song":
                # this is stored in args4, search query for arg4[genre name]
                # return all elements of given genre
                ##### This is a foreign query one ######
                return database_operator.foreign_query('NAME', 'SONGS', 'GENRE', args[3])
    #elif args[0] == "name":
        if args[1] == "genre":
            if args[2] == "artist":
                # this is stored in args4 , search query for arg4 [genre name]
                # return all elements of given genre
                return database_operator.query('NAME', 'ARTISTS', 'GENRE', args[3])
    #elif args[0] == "name":
        if args[1] == "length":
            if args[2] == "song":
                # this is stored in arg4 , search query for arg34[song name]
                # return all songs of a given length
                return database_operator.query('NAME', 'SONGS', 'LENGTH', args[3])

    elif args[0] == "length":
        if args[1] == "song":
            # this is stored in arg3 , search query for arg3 [song name]
            # return the length of song
            return database_operator.query('LENGTH', 'SONGS', 'NAME', args[2])

    # For Debugging
    #return '[Developer] Error: Nothing Happened [{}]'.format(args)

    raise InvalidCommand
    #return "Incorrect commands entered"
