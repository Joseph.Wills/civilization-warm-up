"""
    Helps split and format command with regards to quotation marks.
"""
import re


def split_command(command):
    c = ""
    try:
        c = re.findall('".+?"', command)[0]
        command = command.replace(c, "")
        c = c.strip('\"')
    except:     #if there are no quotations
        pass
    args = command.split()
    if c is not "":
        args.append(c)
    return args
