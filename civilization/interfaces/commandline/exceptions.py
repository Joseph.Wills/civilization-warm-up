"""

    Custom exceptions to be used for commandline interface.

"""


class Quit(Exception):
    """
        Raised when user would like to quit.
    """
    def __init__(self):
        self.goodbye_message = "See ya later :)"

    def __str__(self):
        return self.goodbye_message


class LoadDataFailed(Exception):
    """
        Raised when load data can't load the database.
    """
    def __init__(self):
        self.err_message = "ERROR: Load Data Failed\n"

    def __str__(self):
        return self.err_message


class DataNotLoaded(Exception):
    """
        Raised when database has not been previously loaded.
    """
    def __init__(self):
        self.err_message = "ERROR: Database has not been loaded. "   \
                           "Call 'load data' to load it before making " \
                           "a query\n"

    def __str__(self):
        return self.err_message


class QueryNotFound(Exception):
    """
        Raised when query is not found in the database.
    """
    def __init__(self):
        self.err_message = "ERROR: Query not found\n"

    def __str__(self):
        return self.err_message


class InvalidCommand(Exception):
    """
        Raised when command is not valid.
    """
    def __init__(self):
        self.err_message = "ERROR: Command not valid. See 'help' for more info\n"

    def __str__(self):
        return self.err_message


