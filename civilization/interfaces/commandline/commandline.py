"""

    Commmandline interface for Civilization.

"""
from civilization.interfaces.commandline.exceptions import Quit
from civilization.database_interaction.database_operations import DatabaseOperations
from civilization.parsing.parser import parser_function
import sys


database_operator = DatabaseOperations()


def run_commandline():
    """
        Main function called when running Civilization
        in commandline.
    """
    try:
        setup()
        run()
    except Quit as bye:
        exit(bye)


def run():
    """
        Keep getting and parsing user input until
        user quits.
    """
    while True:
        try:
            parse_command(database_operator)
        except KeyboardInterrupt:
            raise Quit
        except Quit:
            raise Quit
        except Exception as err:
            print(err)


def parse_command(database_operator):
    """
        Get command from user and parse the result.
    """
    print('\n' + parser_function(input(), database_operator) + '\n')


def setup():
    """
        Setup commandline interface environment.
    """
    print('Welcome to Civilization! Glad you\'re here.\n')


def exit(goodbye):
    """
        Display goodbye message and exit.
    """
    print(goodbye)
    database_operator.close_connection()
    sys.exit()
