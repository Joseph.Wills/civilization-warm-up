import csv, sqlite3
from pathlib import Path
from civilization.interfaces.commandline.exceptions import LoadDataFailed


DIRECTORY = Path(__file__).absolute().parent
DATABASE = str(DIRECTORY / 'parser_database.db')
ARTISTS = str(DIRECTORY / 'artist_dataset.csv')
SONGS = str(DIRECTORY / 'song_dataset.csv')
'''
    load_database.py loads the two datasets into a database with two tables. It contains an artist table and a song table.
    
    The names of the tables comply with the names in the database_types.py file (SONGS and ARTISTS)
    
    The fields of the two csv files are loaded into the database called parser_database.db
'''


def load_database():

    try:
        Path(DATABASE).unlink()

        conn = sqlite3.connect(DATABASE)
        conn.text_factory = str
        cur = conn.cursor()
        cur.execute('''CREATE TABLE IF NOT EXISTS SONGS(ID PRIMARY KEY , Name TEXT, Length INTEGER);''')
        cur.execute('''CREATE TABLE IF NOT EXISTS ARTISTS(ID PRIMARY KEY, Name TEXT, Genre TEXT);''')

        artists = csv.reader(open(ARTISTS, 'r'))
        for row in artists:
            to_art = [row[0], row[1], row[2]]
            cur.execute("INSERT INTO ARTISTS(ID,Name,Genre) VALUES (?,?,?);", to_art,)
        conn.commit()

        songs = csv.reader(open(SONGS, 'r'))
        for row in songs:
            to_song = [row[0], row[1], row[2]]
            cur.execute("INSERT INTO SONGS(ID,Name,Length) VALUES (?,?,?);", to_song,)
        conn.commit()

        conn.close()

        return 'Database Loaded'
    except:
        raise LoadDataFailed







