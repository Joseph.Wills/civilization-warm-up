"""

    Custom data types for interacting with the database.

"""
from enum import Enum


class Tables(Enum):
    """
        Data types to represent tables in database.

        Attributes
        ----------
        ARTISTS : str
            Represents Artists table
        SONGS : str
            Represents Songs table
    """
    ARTISTS = 'ARTISTS'
    SONGS = 'SONGS'
