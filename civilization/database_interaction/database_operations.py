"""

    Database operations for interacting with the database.

"""
import sqlite3
from pathlib import Path
from civilization.interfaces.commandline.exceptions import QueryNotFound
from civilization.interfaces.commandline.exceptions import DataNotLoaded
from civilization.database_interaction.database_types import Tables
from civilization.database_interaction.load_database import load_database



class DatabaseOperations:

    def __init__(self):
        self.DB_PATH     = Path(__file__).absolute().parent /'parser_database.db'
        self.ARTISTS     = Tables.ARTISTS.name
        self.SONGS       = Tables.SONGS.name
        self.TABLES      = [self.ARTISTS, self.SONGS]
        self.connection  = None
        self.table       = None
        self.other_table = None

    def set_connection(self):
        """
            Set connection to the database.
        """
        self.connection = sqlite3.connect(str(self.DB_PATH))

    def check_connection(self):
        """
            Check if connection is running and open
            if it is not.
        """
        if self.connection is None:
            self.set_connection()
        else:
            assert self.connection is not None, '[Developer] connection not established'

    def load(self):
        """
            Call load_database.
        """
        return load_database()

    def query(self, query, table, item_type, item):
        """
            Perform a query on the database.
        """
        self.check_if_loaded()
        self.check_connection()
        cur = self.connection.cursor()
        self.set_tables(table)

        cur.execute(
            '''SELECT {} '''
            '''FROM {} '''
            '''WHERE {} == ?'''
            .format(
                query, self.table, item_type
            ),
            (item,)
        )

        self.reset_tables()
        try:
            return self.filter_results(cur.fetchall())
        except:
            raise QueryNotFound

    def foreign_query(self, query, table, item_type, item):
        """
            Perform a foreign key query on the database.
        """
        self.check_if_loaded()
        self.check_connection()
        cur = self.connection.cursor()
        self.set_tables(table)

        cur.execute(
            '''SELECT {}.{} '''
            '''FROM {} '''
            '''INNER JOIN {} '''
            '''ON ARTISTS.ID == SONGS.ID '''
            '''WHERE {}.{} == ?'''
                .format(
                table, query, self.table, self.other_table,
                self.other_table, item_type
            ),
            (item,)
        )

        self.reset_tables()
        try:
            return self.filter_results(cur.fetchall())
        except:
            raise QueryNotFound

    def filter_results(self, fetch_list):
        """
            Filter list results and output
            as strings.
        """
        tuples_rmvd =  [fetched[0] for fetched in fetch_list]
        if not tuples_rmvd:
            raise QueryNotFound

        return ', '.join(str(result) for result in tuples_rmvd)

    def check_if_loaded(self):
        """
            Check if database has been loaded.
        """
        try:
            open(self.DB_PATH)
        except:
            raise DataNotLoaded

    def set_tables(self, table):
        """
            Set the tables to be used.
        """
        assert table in self.TABLES, '[Developer] Error: table not in Tables'
        self.table = table
        self.set_other_table()

    def set_other_table(self):
        """
            Set second table if foreign query is used.
        """
        if self.table == self.ARTISTS:
            self.other_table = self.SONGS
        else:
            self.other_table = self.ARTISTS

    def reset_tables(self):
        """
            Reset active tables.
        """
        self.table       = None
        self.other_table = None

    def close_connection(self):
        """
            Close connection to the database.
        """
        if self.connection is not None:
            self.connection.close()
